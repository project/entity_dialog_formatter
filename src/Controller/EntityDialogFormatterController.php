<?php

namespace Drupal\entity_dialog_formatter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a route controller for render entities.
 */
class EntityDialogFormatterController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_mananer, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_mananer;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Handler the rendering of the entities.
   */
  public function render(Request $request, $type, $view_mode, $id, $theme, $title) {
    $storage = $this->entityTypeManager->getStorage($type);
    $ids = Json::decode($id);
    $entities = $storage->loadMultiple($ids);

    $build = [
      '#theme' => $theme,
      '#entities' => [],
    ];

    foreach ($entities as $entity) {
      if ($entity->access('view', $this->currentUser)) {
        $view_builder = $this->entityTypeManager->getViewBuilder($type);
        $build['#entities'][] = $view_builder->view($entity, $view_mode);
      }
    }

    return $build;
  }

  /**
   * Handler for the title of the dialog.
   */
  public function getTitle(Request $request, $type, $view_mode, $id, $theme, $title) {
    return $title;
  }

}
