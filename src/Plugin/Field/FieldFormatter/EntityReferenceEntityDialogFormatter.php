<?php

namespace Drupal\entity_dialog_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Markup;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_dialog_entity_view",
 *   label = @Translation("Dialog rendered entity"),
 *   description = @Translation("Display the referenced entities rendered in a dialog."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceEntityDialogFormatter extends EntityReferenceEntityFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_mode_destination' => 'default',
      'list_theme' => 'entity_dialog_formatter_list',
      'link_class' => 'use-ajax',
      'dialog_width' => '800',
      'dialog_height' => '',
      'dialog_type' => 'modal',
      'dialog_title' => '',
      'display_all_dialog' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['view_mode_destination'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('View mode in dialog'),
      '#default_value' => $this->getSetting('view_mode_destination'),
      '#required' => TRUE,
    ];

    $elements['list_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List theme name'),
      '#default_value' => $this->getSetting('list_theme'),
      '#required' => TRUE,
    ];

    $elements['link_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#default_value' => $this->getSetting('link_class'),
      '#required' => TRUE,
    ];

    $elements['dialog_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog type'),
      '#default_value' => $this->getSetting('dialog_type'),
      '#required' => TRUE,
    ];

    $elements['dialog_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog width'),
      '#default_value' => $this->getSetting('dialog_width'),
      '#required' => FALSE,
    ];

    $elements['dialog_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog height'),
      '#default_value' => $this->getSetting('dialog_height'),
      '#required' => FALSE,
    ];

    $elements['dialog_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dialog title'),
      '#default_value' => $this->getSetting('dialog_title'),
      '#required' => FALSE,
    ];

    $elements['display_all_dialog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display all entities in dialog'),
      '#default_value' => $this->getSetting('display_all_dialog'),
      '#required' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $view_modes = $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type'));
    $view_mode = $this->getSetting('view_mode_destination');
    $summary[] = $this->t('Rendered as @mode in dialog', ['@mode' => $view_modes[$view_mode] ?? $view_mode]);

    $list_theme = $this->getSetting('list_theme');
    $summary[] = $this->t('List theme name: @list_theme', ['@list_theme' => $list_theme]);

    $link_class = $this->getSetting('link_class');
    $summary[] = $this->t('Link class: @link_class', ['@link_class' => $link_class]);

    $dialog_type = $this->getSetting('dialog_type');
    $summary[] = $this->t('Dialog type: @dialog_type', ['@dialog_type' => $dialog_type]);

    $dialog_width = $this->getSetting('dialog_width');
    $summary[] = (!empty($dialog_width)) ? $this->t('Dialog options: @dialog_width', ['@dialog_width' => $dialog_width]) : $this->t('No dialog width');

    $dialog_height = $this->getSetting('dialog_height');
    $summary[] = (!empty($dialog_height)) ? $this->t('Dialog options: @dialog_height', ['@dialog_height' => $dialog_height]) : $this->t('No dialog height');

    $dialog_title = (!empty($this->getSetting('dialog_title'))) ? $this->getSetting('dialog_title') : $this->t('None');
    $summary[] = $this->t('Dialog title: @dialog_title', ['@dialog_title' => $dialog_title]);

    $display_all = ($this->getSetting('display_all_dialog')) ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Display all items in dialog: @display_all_dialog', ['@display_all_dialog' => $display_all]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view_mode = $this->getSetting('view_mode');
    $view_mode_dest = $this->getSetting('view_mode_destination');
    $list_theme = $this->getSetting('list_theme');
    $link_class = $this->getSetting('link_class');
    $dialog_type = $this->getSetting('dialog_type');
    $dialog_width = $this->getSetting('dialog_width');
    $dialog_height = $this->getSetting('dialog_height');
    $display_all_dialog = $this->getSetting('display_all_dialog');

    $dialog_options = [];
    if (!empty($dialog_width)) {
      $dialog_options['width'] = $dialog_width;
    }
    if (!empty($dialog_height)) {
      $dialog_options['height'] = $dialog_height;
    }

    $elements = [];

    $entities = $this->getEntitiesToView($items, $langcode);
    $entities_id = [];
    foreach ($entities as $entity) {
      $entities_id[] = $entity->id();
    }

    foreach ($entities as $entity) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
      $entity_array = $view_builder->view($entity, $view_mode);
      $output = \Drupal::service('renderer')->render($entity_array);
      $filtered_output = $this->filterLinksFromHtml($output);

      $elements[] = [
        '#title' => Markup::create($filtered_output),
        '#type' => 'link',
        '#url' => Url::fromRoute(
          'entity_dialog_formatter.dialog_renderer',
          [
            'type' => $entity->getEntityTypeId(),
            'view_mode' => $view_mode_dest,
            'id' => ($display_all_dialog) ? Json::encode($entities_id) : Json::encode([$entity->id()]),
            'theme' => $list_theme,
            'title' => $this->getDialogTitle($entities, $entity),
          ]
        ),
        '#attributes' => [
          'class' => explode(' ', $link_class),
          'data-dialog-type' => $dialog_type,
          'data-dialog-options' => Json::encode($dialog_options),
        ],
        '#attached' => [
          'library' => [
            'core/drupal.dialog.ajax',
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * Function to remove potential links from the HTML.
   *
   * @param string $string
   *   An HTML string.
   *
   * @return string
   *   The filtered HTML.
   */
  private function filterLinksFromHtml($string) {
    $filtered_html = preg_replace('@<(a)\b.*?>.*?</\1>@si', '', $string);
    return $filtered_html;
  }

  /**
   * Function to get the title of the dialog.
   *
   * @param array $entities
   *   The entities of the entity reference field.
   * @param object $entity
   *   The entity to display.
   *
   * @return string
   *   The title of the dialog.
   */
  private function getDialogTitle(array $entities, $entity) {
    $dialog_title = $this->getSetting('dialog_title');
    $display_all_dialog = $this->getSetting('display_all_dialog');

    if (empty($dialog_title)) {
      if (count($entities) === 1 || !$display_all_dialog) {
        $dialog_title = $entity->label();
      }
      else {
        $dialog_title = $this->fieldDefinition->getItemDefinition()->getFieldDefinition()->getLabel();
      }
    }
    return str_replace('/', '-', $dialog_title);
  }

}
