# Entity Dialog Formatter

The entity_dialog_formatter module provides a new entity reference formatter
that allows you to display entities into a dialog.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_dialog_formatter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_dialog_formatter).


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The requirement is that you already have installed the module field from the
Drupal core.


## Installation

This is like any other module.

1. Add it to your project with composer

   `composer require drupal/entity_dialog_formatter`.

2. Enable the module


## Configuration

1. Pick the "Dialog rendered entity" in your display mode for your entity
   reference field and then configure the different options that the formatter
   allows you to change.
2. Grant the 'Render entity dialog' permission to the roles that will use
   the entity dialog formatter.
3. Grant the permission to view the entities that you will be displayed in the
   dialog for the roles that will use the entity dialog formatter.
   Example: View published content


## Maintainers

- [Philippe Joulot](philippe.joulot@laposte.net)
